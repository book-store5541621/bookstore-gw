package com.example.bookstoreapigw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookstoreApiGwApplication {

    public static void main(String[] args) {
        SpringApplication.run(BookstoreApiGwApplication.class, args);
    }

}

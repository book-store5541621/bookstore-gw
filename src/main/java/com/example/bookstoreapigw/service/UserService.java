package com.example.bookstoreapigw.service;

import com.example.bookstoreapigw.dto.request.UserRequestDto;
import com.example.bookstoreapigw.dto.response.UserResponseDto;

import java.util.List;

public interface UserService {
    void insert(UserRequestDto userRequestDto);

    void update(Integer id, UserRequestDto userRequestDto);

    void delete(Integer id);

    UserResponseDto getById(Integer id);

    List<UserResponseDto> getAll();
}

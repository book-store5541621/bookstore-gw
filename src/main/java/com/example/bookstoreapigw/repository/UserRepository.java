package com.example.bookstoreapigw.repository;

import com.example.bookstoreapigw.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {
    Optional<User> findUserInfoByUsernameIgnoreCase(String username);


}


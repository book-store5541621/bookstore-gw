package com.example.bookstoreapigw.dto.response;

import java.time.LocalDateTime;

public record UserPostResponseDto(String title, String content, LocalDateTime createdDate) {
}

package com.example.bookstoreapigw.dto.response;

public record UserResponseDto(String username, String email) {

}
